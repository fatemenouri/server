const spinner = document.getElementById("spinner");
function getData(url = "") {
    showSpinner();
    const response = fetch(url, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
    })
        .then((response) => response.json())
        .then((data) => {
            createHtml(data);
        });
}

function cities(url = "") {
    const response = fetch(url, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
    })
        .then((response) => response.json())
        .then((data) => {
            setForm(data.list);
        });
}

function createHtml(data) {
    document.getElementById("city").innerHTML = data.name;
    document.getElementById("weather").innerHTML = data.weather[0].description;
    document.getElementById("degree").innerHTML = parseInt(data.main.feels_like);
    document.getElementById(
        "img"
    ).src = `./icons/${data.weather[0].description}.png`;
}

function setForm(data) {
    select = document.getElementById("cities");
    for (i = 0; i < data.length; i++) {
        var option = data[i].name;
        var element = document.createElement("option");
        element.textContent = option;
        element.value = option;
        select.appendChild(element);
        var value = select.options[select.selectedIndex].value;
    }
}

function updateSelector() {
    var select = document.getElementById("cities");
    var option = select.options[select.selectedIndex];
    select.value = option.value;
    updateCard(select.value);
}

function updateCard(cityName) {
    showSpinner();
    const response = fetch(
        "https://api.openweathermap.org/data/2.5/box/city?bbox=50.071564,35.227849,52.157593,36.033727,10&appid=73484e0f6c5d3d26e3b290d9c719bb45",
        {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
        }
    )
        .then((response) => response.json())
        .then((data) => {
            for (var j = 0; j < data.list.length; j++) {
                if (data.list[j].name === cityName) {
                    createHtml(data.list[j]);
                }
            }
        });
}

function showSpinner() {
    spinner.classList.add("show");
    setTimeout(() => {
        spinner.classList.remove("show");
    }, 500);
}

cities(
    "https://api.openweathermap.org/data/2.5/box/city?bbox=50.071564,35.227849,52.157593,36.033727,10&appid=73484e0f6c5d3d26e3b290d9c719bb45"
);
getData(
    "https://api.openweathermap.org/data/2.5/weather?q=shahriar&appid=73484e0f6c5d3d26e3b290d9c719bb45"
);
